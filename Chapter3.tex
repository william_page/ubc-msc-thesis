 % !TEX root =  ./thesis1.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% An example section.
%You should start all sections with \section{SectionName}, which is what will show up
%as the section header, and will be automatically included in the ToC.
 %If you want to reference the section later you should also use
%\label{sec:sectionName}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Semiconductor Detector Physics}


\begin{table}
\begin{tabular}{| l | c | c | c | c |}
\hline
& \textbf{CDMS} & \textbf{SCDMS} & \textbf{SCDMS} \\
& \textbf{II} & \textbf{Soudan} & \textbf{SNOLAB}\\ \hline
Detector & ZIP & iZIP & iZIP  (HV)\\ \hline
Mass per Detector [kg] & 0.25 & 0.62 & 1.38 (1.38) \\ \hline
Number of Detectors & 19 & 15 & $\sim$42 ($\sim$6) \\ \hline
Total Ge Mass [kg] & 4.75 & 8.89 & $\sim$58 ($\sim$8)\\ \hline
Phonon Channels per Det. & 4 & 8 &12 ($\sim$16) \\ \hline
Phonon Energy Res. [eV] & 180 & 200 &  $\sim$75 ($\sim$50) \\ \hline 
Trigger Threshold [eV] & $\sim$2000 & $\sim$3000 & $\sim$550 ($\sim$350) \\ \hline
Charge Energy Res. [eV] & 300 & 450 & $\sim$200 (--) \\ \hline
\end{tabular}
\caption{Detector specs for different generations of the Germanium CDMS detectors. Adapted from \cite{hertel} and SuperCDMS SNOLAB detector performance projections, by permission. \label{specs1}}
\end{table}


The CDMS detector bulk is ultra pure crystalline germanium and silicon.  The response of cryogenically cooled crystalline semiconductors to a nuclear recoil versus an electron recoil provides CDMS with the ability to discriminate between a potential WIMP signal and background events. Additionally, the availability of radioactively stable Ge and Si, free of radioactive contaminants, ensures that any energy deposited in the crystal is the result of a foreign particle collision. Si and Ge also have good charge transport properties and small band gaps \cite{knoll}, the importance of which will be elaborated on in section \ref{part_int}.  

As shown in Table \ref{specs1}, the mass of each individual detector is on the order of one kg and they are about the size of a hockey puck (fig. \ref{fig:detector}). The detectors are stacked in towers, each of which (for SuperCDMS SNOLAB) holds 6 detectors.

\begin{figure}[h!]
\centering
   \includegraphics[width=7cm]{iZIP_6.png}
   \caption{A SuperCDMS SNOLAB iZIP 6 detector. Figure from SuperCDMS collaboration standard public plots \cite {CDMS_plots}.
   \label{fig:detector}}
\end{figure}

Ever since 2003, CDMS has been operating out of the Soudan mine ($\sim$2300ft below surface) in Minnesota and the SuperCDMS SNOLAB experiment is scheduled to start taking data in the SNOLAB facility ($\sim$6800ft below surface) in 2018.  The earth above these facilities serves as a shield of cosmic ray muons.

Additional shielding is added locally around the detectors.  The towers sit in a muon veto cage in the event that a cosmic ray muon does penetrate into the facility.  Ancient, non-radioactive, lead surrounds the detectors and serves to absorb gamma rays while a polyethylene cover is in place to absorb radioactive neutrons. A copper enclosure shields from alpha and beta radiation.

Because any particle (WIMP or background) that interacts within the detector does so in (1) an electron recoil or (2) a nuclear recoil, we now discuss the basic physics of these interactions and why they form the basis of the detectors' sensitivity to WIMPs. The ideas behind this discussion of nuclear vs. electron recoils in semiconductors come from Jan Lindhard's work, published in the 1960's, which is known as Lindhard theory \cite{lindhard}.


\subsection{Electron Recoils}
\label{part_int}
Most background particles ($\alpha, \beta, \gamma)$ are far more likely to recoil off electrons in the detector bulk. For example, when a medium energy $\gamma$-ray (10keV to 1MeV) passes through the detector bulk, it is likely to compton scatter and create $e^-/h+$ pairs along its track\cite{Jeff_thesis}. In germanium, where the energy to create an $e^-/h+$ pair ($E_{create}$) is 2.96eV, a 10keV $\gamma$ produces ~3000 $e^-/h+$ pairs under the good assumption that it is fully absorbed. Similarly, $\alpha$ and $\beta$ background recoil off electrons and ionize $e^-/h+$ pairs in equal proportions to their energy.

A recoiling electron will lose energy by transferring it to other surrounding electrons.  Fig. \ref{fig:e_stop} gives the stopping power for recoiling electrons in Si (which is similar to that in Ge). Note that on this plot the x and y energy scales are comparable, and the y axis gives the energy loss per $\mu$m.  Equivalently, all but the highest energy ($>10^5$eV) recoiling electrons only travel at maximum a matter of $\mu$m before losing most of their energy.  We can be confident in the assumption that all of a recoiling electron's energy will be deposited within the crystal.


\begin{figure}[h!]
\centering
   \includegraphics[width=7cm]{e_stop.png}
   \caption{Electron stopping power in Silicon.  Internal CDMS figure, used with permission, from \cite{blas}.
   \label{fig:e_stop}}
\end{figure}


How does the recoiling electron lose its energy to other electrons?  An electron with sufficient energy to create another $e^-/h+$ pair ($E_{create}$) will do so until its energy falls below $E_{create}$.  The newly ionized electrons will similarly ionize other $e^-/h+$ pairs, creating an electron cascade.  Energy is distributed to other local electrons until every electron's energy is below $E_{create}$.  At this point, the electrons are still capable of losing energy, but no longer by ionization. Instead they transfer their energy to vibrations in the crystal lattice (phonons), which then travel like particles through the crystal \cite{hertel}. Electrons radiate in this manner until they reach the gap energy ($E_{gap}$) of the material, which is the minimum energy that an excited electron can have.  Therefore, an electron recoil results in two types of energy deposited in the crystal:  $e^-/h+$ pair energy and phonon energy.

We can roughly calculate the expected energies channeled into $e^-/h+$ pair and phonon production.  Given some initial electron recoil energy, the fractional charge energy is given by $E_{gap}/E_{create}$ and the fractional phonon energy given by $1-E_{gap}/E_{create}$.  In germanium, where $E_{gap}$=0.785eV and $E_{create}$=2.96eV, roughly 25\% of electron recoil energy goes into $e^-/h+$ pairs\cite{hertel}.


\subsection{Nuclear Recoils}

WIMPs and neutrons (a particularly toxic background) recoil off Ge and Si nuclei in the CDMS detector bulk.  Nuclear recoils are similar to electron recoils in many ways; however, they differ most importantly in that a lower fraction of energy goes into $e^-/h+$ pairs than in electron recoils. 

When a nucleus recoils, it is capable of transferring energy to other nuclei $and$ other surrounding electrons.  Nuclei are capable of this from a purely kinematic standpoint, whereas electrons are not because of their small mass \cite{lindhard}. Let us consider a high energy nuclear recoil and a low energy nuclear recoil separately in order to discuss how energy is partitioned. In a high energy nuclear recoil ($\sim1$MeV in Ge),  most of the energy is transferred to electrons (figure \ref{i_yield}) which then produces an electron cascade \cite{lindhard}.  Therefore, a high energy nuclear recoil looks a lot like an electron recoil in the detectors.  

\begin{figure}[h]
\begin{center}
\includegraphics[%
  width=1\linewidth,
  keepaspectratio]{i_yield.png}
\end{center}
\caption{The ionization yield of different energy nuclear and electron recoil events.  Ionization yield is defined as the fraction of recoil energy that goes into the $e^-/h+$ pairs with the electron recoil ionization yield normalized to one (as seen by the horizontal lines).  Internal CDMS figure, used with permission, from \cite{hertel}. \label{i_yield}}
\end{figure}

A lower energy nuclear recoil, however, transfers energy more evenly between excitations of electrons and excitations of other nuclei \cite{lindhard}.  The nuclei are freed from the crystal lattice and excite other nuclei in a cascade separate from the electron cascade.  Once the nuclei have insufficient energy to excite another nuclei they lose their energy to phonon production.  This time, however, there is no analogous $E_{gap}$ for phonons as there was in the electron case.  Therefore, nuclear cascades are more efficient than electron cascades in the final phonon production\cite{lindhard}. Figure \ref{i_yield} displays this aspect of nuclear recoils over a range of recoil energy.  

From the discussion of section \ref{detwimps}, clearly it is the lower energy ($<$500keV) nuclear recoils that are expected from WIMPs. The event discrimination in this energy range provides the CDMS detectors with their sensitivity to WIMPs masses above 5GeV.

\section{Amplifiers}

The detector surfaces are instrumented with sensors designed to measure the $e^-/h+$ and phonon energy with optimal resolution. As resolutions improve, event discrimination improves and (perhaps more importantly) detector thresholds can be lowered. 

\subsection{Measuring the $e^-/h^+$ Energy}

Once an electron cascade has occurred, the detectors prevent the excited $e^-/h+$ pairs from de-exciting back into valence states by applying a voltage across the detector.  The electric field drifts the charges to the flat detector endcaps. In standard CDMS iZIP detectors, the voltage has been tuned to the smallest value such that charge trapping in crystal impurities is also minimized. The optimal field is $\sim$1V/cm. In SuperCDMS iZIP detectors, FET (Field Effect Transistor) amplifiers read out the image charge on the electrodes and amplify this signal as a voltage that is read out and further amplified by downstream amplifiers. Because the fall time of the SuperCDMS iZIP amplifier is larger than the charge collection time ($\sim 1 \mu s$), a voltage pulse proportional to the charge collected is the only information that is obtained from the charge amplifier \cite{Jeff_thesis}. SuperCDMS SNOLAB will use HEMTs (High Electron Mobility Transistors) in place of FETs, primarily to reduce heat load on the fridge, but which rely on the same basic amplification principles described above.

\subsection{Measuring the Phonon Energy}

Six phonon channels are implemented on each detector surface, and each channel consists of thousands of Transition Edge Sensors (TESs). The TESs are made from Tungsten superconducting material whose transition temperatures ($T_c$) are tuned in fabrication to be anywhere between 30mK and 200mK.  The voltage biased TESs are held within the range of their transition such that when heat from phonons reach the sensors their resistance changes rapidly and the current through them drops. The current through the TES is inductively coupled to a SQUID which further amplifies the reduction in current. TESs, which are at the heart of the CDMS detectors' sensitivity to dark matter, have also propelled the fields of x-ray astronomy and CMB cosmology to their current states, and the sensor technology is reviewed in K. Irwin's and G. Hilton's seminal chapter \cite{irwin}.

Unlike the charge measurement, more than just the phonon energy is encoded in the phonon measurement. We obtain an estimate of the event location by comparing the energy deposited in the different phonon channels.  We have a good idea for how phonons move through the crystalline bulk because the anisotropic and incoherent propagation of phonons in crystalline semiconductors is well modeled by CDMS's detector Monte Carlo \cite{geant_phonons}.  Therefore the partitioning of energy in the different channels allows a weighted average of sorts to determine the x and y coordinate of the initial event\cite{Jeff_thesis}.  The pulse shape of the phonon energy signal is also occasionally used to obtain more information about the event.


\section{iZIP Interleaved Design}

\begin{figure}[h!]
\centering
   \includegraphics[width=6cm]{cdmsII_det.png}
      \includegraphics[width=5cm]{iZIP_1.png}
   \caption{(Left) A schematic of the CDMS II detector. The four phonon channels are colored on the top surface and the charge electrodes (Qi and Q0) are pictured on the lower surface. Figure from \cite{Jeff_thesis}. (Right)  The iZIP design with interleaved charge electrodes ($\pm2V$) and phonon rails (0V). Internal CDMS figure, used with permission, from \cite{iZIP}.
   \label{cdmsII_det}}
\end{figure}

While the CDMS II ZIP detectors were beautiful devices with world-leading WIMP sensitivity in their time, they suffered primarily from one design flaw. The ionization from electron recoils close to the detector surface was more prone to trapping which reduced the ionization yield of the event \cite{iZIP}. This reduced ionization yield caused surface electron recoils  to mimic the nuclear recoil signature and thus leak into the WIMP signal region. CDMS II sensitivities were limited by this background \cite{iZIP}.


A new detector (the iZIP) was designed to provide a solution to discriminate the surface event background. The ionization electrodes are interleaved between the phonon sensors which allows readout of both ionization and phonon energy on each detector face.  Just as importantly, the phonon TES rails are maintained at 0V while the ionization electrodes are kept at opposite potentials ($\pm$2V in standard operation) on either face.  This configuration produces a unique electric field within the crystal (figure \ref{iZIPs}) wherein the ionization from surface events will largely be collected on one side of the detector face.  The CDMS and EDELWEISS collaborations have shown that the interleaved design allows for robust rejection of surface events.  An analysis cut on asymmetric charge collection on side 1 vs. side 2 of the iZIP rejects the surface events. 

Figure \ref{iZIPs} (right) shows the ionization yield vs. recoil energy for events from 900 hours of exposure of Soudan iZIP T3Z1 to a $^{210}$Pb source. The $^{210}$Pb source was found, as expected, to produce $\sim$ 130 surface electron recoils per hour via beta decay. These events exhibit reduced ionization yield and fail the symmetric charge cut. They populate the region above the $2\sigma$ nuclear recoil band and below ionization yields of $\sim$1.  The events below the germanium nuclear recoil band but also failing the symmetric charge cut are surface events from recoiling $^{206}$Pb nuclei (the end product of the $^{210}$Pb decay).  The colored blue dots are events that pass the symmetric charge charge and accordingly show large ionization yield corresponding to bulk electron recoils. Out of the 90,000 events in this plot, two outliers exist which (barely) pass the charge symmetry cut but show low ionization yield, which are blue and circled in black \cite{iZIP}. Overall, this study demonstrates robust surface rejection capability of the iZIP. 

\begin{figure}[h!]
\centering
   \includegraphics[width=6cm]{iZIP_2.png}
      \includegraphics[width=6.3cm]{yield_sym.png}
   \caption{(Left) The electric field and potential lines produced from the phonon rails (yellow) and charge electrodes (green). Notice that the unique surface E-field extends $\sim$1mm into the crystal and therefore surface events within this margin should exhibit asymmetric charge collection. (Right) Data from T3Z1 showing surface event discrimination (discussion in main text). Internal CDMS figure, used with permission, from \cite{iZIP}.
   \label{iZIPs}}
\end{figure}

\iffalse
\section{CDMS HV detectors}
\label{cdmslite}

In parallel to the R&D phase of the iZIP detector, CDMS detector experts were developing a novel configuration for running existing devices in order to lower thresholds and improve sensitivity to low mass WIMPs. Instead of applying $\sim 4V$ across the detectors in order to drift $e^-/h^+$ pairs to the charge amplifiers, a much larger voltage $\sim70V$ is applied in order amplify the phonon signal of a given event.    Because the high mass ($>$15GeV) sensitivity of the CDMS technology is now order of magnitudes less than liquid noble gas experiments (with their enormous fiducial volumes sans surfaces), this 


Despite the tremendous R&D and WIMP sensitivity results from the 
\fi

% EOF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%