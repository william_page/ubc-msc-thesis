 % !TEX root =  ./thesis1.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% An example section.
%You should start all sections with \section{SectionName}, which is what will show up
%as the section header, and will be automatically included in the ToC.
 %If you want to reference the section later you should also use
%\label{sec:sectionName}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Early Evidence}
\label{sec:evidence}


In the 1920s the Dutch astronomer Jan Oort, observing the motion of stars in the Milky Way, measured the rotational speed of these stars around the galactic center.  By this time, it had been determined that the bulk of the galaxy's luminous mass was at its center and this mass had been experimentally deduced.  The rotational speeds of the stars found by Oort were significantly greater than those given by Newton's law of gravitation \cite{Oort}.

In 1933 Fritz Zwicky found the same discrepancy between expected and observed galactic rotational speeds in his observations of the Coma galaxy cluster. He computed the mass of the Coma cluster by way of the virial theorem and found it to be 400 times larger than the mass due to the luminous part alone. Zwicky called this the ``missing mass'' problem \cite{Zwicky}.

Since the 1930s more accurate measurements have been made of the rotation speeds of stars in our own and other galaxies. Vera Rubin pioneered the work on these measurements  in her observations of the Andromeda galaxy in the 1970s.  She represented her data as plots of stellar rotational speed vs. distance from galactic center, known as rotational curves, shown in fig. \ref{fig:rotational_curves}. Rubin is attributed with publishing the first robust evidence for dark matter and solidifying Zwicky's, Oort's, and others' earlier predictions of the existence of dark matter \cite{rubin2}.

\begin{figure}[h]
  \centering
   \includegraphics[width=12.5cm]{rotation_curve.png}
   \caption{Rotational curves, similar to those published by Rubin, overlaid on an image of the Andromeda galaxy (M31) taken by the Palomar Sky Survey. The roughly constant velocity at high radius indicates that the high-luminosity galactic center makes up a small fraction of the total mass. Figure from \cite{rubin2}. \label{fig:rotational_curves}}
   
\end{figure}

If all matter in the galaxy were luminous and roughly estimated to be concentrated at the center, Newton's laws of gravitation give the rotational speed of the stars as:
$$v(r)=\sqrt{GM}\sqrt{\frac{1}{r}}.$$


\noindent
From Rubin and other more recent measurements (e.g.  \cite{Sofue}) we know that the rotation speed of stars does not fall off as $r^{-1/2}$. Rather speeds are roughly constant at large radius from the galactic center, suggesting that luminous matter makes up only a small portion of the total mass and that non-luminous ``dark'' matter is distributed around the galaxy.  


If we instead modeled mass in the galaxy as a spherically symmetric distribution that varies with distance, $M(r)$, the constant stellar rotational velocity at large distances given by $v=C=\sqrt{GM(r)}\sqrt{\frac{1}{r}}$, can be explained by an enclosed mass that varies linearly with the distance from the center:
$$M(r) \propto r.$$
Therefore the enclosed mass has a density proportional to the inverse square of the distance from the galactic center, $\rho(r) \propto \frac{1}{r^2}$. Most of this matter must be dark because the luminous mass density falls off much more rapidly. To account for this missing matter, the leading model is a spherically symmetric ``halo'' of dark matter distributed throughout our galaxy and other galaxies, interacting gravitationally and making up the majority of mass in the galaxy and the universe.

\section{Galaxy Clusters}
\label{sec:galaxy_clusters}
Measurements of galaxy clusters since Zwicky have provided additional evidence for dark matter and important information regarding the composition of dark matter.  Observations of galaxy clusters---the largest gravitationally collapsed astrophysical structures---are particularly powerful because different techniques can be used to make independent measurements of their mass. 

The Chandra Observatory measures the x-ray emission from the intergalactic gas of galaxy clusters. With the clusters' gravitational potential largely due to dark matter, their intergalactic gas gains kinetic energy and heats to $\sim10^8$K, and they are therefore among the brightest x-ray sources \cite{Chandra1}. By measuring the x-rays, the temperature and pressure of the gas can be computed.  If the cluster is in equilibrium (which excludes merging or colliding clusters), the gravitational potential can be computed under the good assumption that gravitational forces cancel with pressure forces. Recent publications matching models of the interstellar gas and dark matter halo to Chandra Observatory data indicate that ordinary matter makes up 12-15\% of the total mass of the cluster \cite{Chandra1} \cite{Chandra2}. Figure \ref{fig:abel1} (left) shows Abell 1689, the largest known galaxy cluster which contains $\sim$1000 galaxies. The intergalactic gas, emitting in the the x-ray, is shown in purple. The optical image is overlaid and shows some distortion and arcing of luminous background objects characteristic of strong gravitational lensing. 

In an independent measurement of the cluster mass, the Hubble Space Telescope measures light from background objects that bends around the massive structures.  The total gravitational mass of the cluster is computed by the strength of the gravitational lensing, which confirms that the majority of gravitational mass of the clusters is not due to the luminous matter\cite{HST1}.

\begin{figure}[h]
  \centering
   \includegraphics[width=5cm]{abel_1689.png}
    \includegraphics[width=7cm]{bullet_cluster.png}
  
   \caption{(Left) Abell 1689 (\textcircled{c} 2008 X-ray: NASA/CXC/MIT/E.-H Peng et al; Optical: NASA/STScI, by permission) \cite{Chandra3}. (Right)  1E 0657-56 (\textcircled{c} 2004 X-ray: NASA/CXC/CfA/M.Markevitch et al.; Optical: NASA/STScI; Magellan/U.Arizona/D.Clowe et al.; Lensing Map: NASA/STScI; ESO WFI; Magellan/U.Arizona/D.Clowe et al., by permission) \cite{markevitch} \cite{clowe} \label{fig:abel1}.}
\end{figure}

The ``Bullet" Cluster is one of the most famous examples of the dark matter making itself apparent in our universe. The Hubble Space Telescope and Chandra Observatory have observed the collision of two galaxy clusters and measured the distributions of both the gravitational matter from strong gravitational lensing and luminous matter from x-ray emission.  As shown in figure  \ref{fig:abel1} (right), these two distributions are observed to have separated. The luminous matter (pink) is superimposed on the distribution of gravitational matter (i.e. predominantly dark matter) as measured by lensing (purple).  The two dark matter distributions have passed through each other while the luminous distributions lag behind due to the impedance of their collisions. The nearly non-interacting dark matter streams through unimpeded. 

\section{The Cosmic Microwave Background}
\label{sec:cmb}

If we follow this chapter's progression to physically larger scales, from the individual galaxies of section \ref{sec:evidence} to the galaxy clusters of section \ref{sec:galaxy_clusters}, then the Cosmic Microwave Background (CMB) is certainly the next topic to discuss. Approximately 400,000 years after the Big Bang, the expanding universe cooled to a point where it became energetically favorable for the plasma of protons and electrons to fall out of equilibrium with photons and form neutral hydrogen. At this point of `recombination,' the universe became transparent to the photons which make up the CMB radiation, which matches a blackbody spectrum with (currently) a temperature of 2.7K. Slight differences, or anisotropies, in this temperature across the sky, on the order of 10$\mu$K, have been a rich source of cosmological information, including the most accurate measure of the non-baryonic (dark matter) matter density in the universe. 

\begin{figure}[h!]
  \centering
   \includegraphics[width=12.5cm]{anisotropy.png}
   \caption{CMB power spectrum predicted by a $\Lambda$-CDM cosmology (i.e. a dark energy (69\%) and cold dark matter (26\%) dominated universe). Data points in red are measurements by the Planck collaboration \cite{planck}. Power spectrum of temperature fluctuations in the Cosmic Microwave Background (\textcircled{c} 2013 ESA/Planck, and the Planck collaboration, from Planck 2013 results. I. Overview of products and results, by permission). \label{fig:anis}}
\end{figure}

Transforming the spacial anisotropies into spherical harmonics (and taking the magnitude) gives the CMB power spectrum. The acoustic peaks of the power spectrum show the angular scales at which the photons were slightly hotter and denser than average at recombination (and today).  The hotter overdensities are well understood as regions where the photons, coupled to the baryonic matter right up to the point of recombination, had clumped together because of gravitational wells into which the baryonic matter were attracted.  

Most importantly for the case of non-baryonic dark matter, the size of the peaks would be much smaller if these gravitational wells had been formed by baryonic matter alone. This is because the pressure of the photons coupled to the baryonic matter opposes the formation of gravitational wells. In order to accurately model the location and size of the peaks, a decoupled non-baryonic matter component must exist which continues to collapse regardless of the photon restoring force.  The photon pressure restoring force does set up an oscillation of the baryon-photon plasma, which is highly sensitive to the non-baryonic matter density, and which is imprinted on the CMB at last scattering in the form of the peaks in the power spectrum \cite{dodelson}.

While the baryonic and non-baryonic matter densities are degenerate with other cosmological parameters in determining the location and size of the peaks, the degeneracies can be broken by including other cosmological data (e.g. 21cm and supernovae measurements).  The best current measurement of the cold dark matter density fraction of the universe is 26\% (with a 69\% dark energy component) \cite{planck}.

\section{Composition Hypotheses}
\label{comp_hyp}

Despite overwhelming observational evidence that dark matter does exist, very little is known about its composition.  A number of theories have been put forth.

In one effort to account for the dark matter, collaborations searched for hidden massive compact halo objects (MACHOs), such as black holes or massive non-luminous planets. They looked for MACHOs in the Milky Way by waiting for slight unexpected gravitational lensing of distant luminous galaxies as a MACHO passed between us and the galaxy.  These searches ruled out the possibility of MACHOs constituting any more than 25\% of the Milky Way's dark matter halo, and therefore disqualified them as the primary dark matter candidate \cite{Macho2}.

Most theories predict that non-baryonic particles make up dark matter halos around galaxies, but still there exist many possibilities for the type of particle.  If we assume that dark matter is non-baryonic, it is highly likely that such dark matter is also non-relativistic, or ``cold",  dark matter. Relativistic, or ``hot," dark matter conflicts with the accepted model of galaxy formation \cite{neutrino}. Returning to the discussion of section \ref{sec:cmb}, had the dark matter been relativistic then its kinetic energy would have largely prevented its gravitational collapse. However, the gravitational landscape at recombination is well understood, and not only is it imprinted on the CMB but it also explains the formation of the dense small scale structures (galaxies) seen in the universe today.  That the majority of dark matter is cold rules out a hot relativistic (neutrino-like) species from contributing substantially to the 26\% dark matter component.

With these requirements, Axions and WIMPs are the two leading dark matter candidates.  The axion particle was originally proposed as a solution to the strong CP problem \cite{wein} and has since become an leading ultra-light mass (100keV to 1MeV) dark matter candidate. The ADMX experiment searches for axions by hoping to detect axion to photon conversion in a strong magnetic field resonance cavity from their lab at the University of Washington \cite{ADMX}.

The SuperCDMS experiment, along with many competitor experiments, searches for WIMPs.  We will devote the remainder of this chapter and chapter 2 to discussion of this hypothetical particle.



\subsection{The WIMP Hypothesis}

The WIMP hypothesis is intriguing because it fits into a parameter space supported by supersymmetric (SUSY) theory as well as cosmology. By satisfying the above cosmological constraints, it is likely that WIMPs are a thermal relic of the Big Bang phase of the universe, that ``froze out" of the primordial plasma at early times. The dark matter density at freeze out (and today) is highly sensitive to the the WIMP cross section $\sigma_{\chi \bar{\chi}}$.  For WIMPs to make up the 26\% energy density of the universe, the WIMP cross section must be on the scale of the weak force where SUSY postulates that new particles could exist.

\begin{figure}[h!]
\centering
   \includegraphics[width=8cm]{freezeout.png}
   \caption{Number density of WIMPs in the Universe as a function of time, where the relic density depends on the WIMP annihilation cross section, $\sigma_{\chi \bar{\chi}}$ or $\sigma_{A}$ in the figure. Evolution of a typical WIMP number density in the early universe (\textcircled{c} NASA/IPAC Extragalactic Database (NED) which is operated by the Jet Propulsion Laboratory, California Institute of Technology, under contract with the National Aeronautics and Space Administration, by permission).
   \label{fig:abundance}}
\end{figure}

A number of assumptions regarding matter in the early universe allows cosmologists to estimate the WIMP cross section and mass.  First, WIMPs would have been in constant creation and annihilation until some critical point of the universe's cooling where the low temperature would prevent any further WIMP creation.  Following this, expansion of the universe would have made it exponentially unlikely that a WIMP would collide with its antiparticle and annihilate \cite{Griest}. This second critical moment---when annihilation ceases---determines particle abundance and depends on the WIMP annihilation cross section as shown in Fig. \ref{fig:abundance}.  In order to account for the dark matter in the universe, the WIMP annihilation cross section is estimated to be roughly at the scale of the weak force where yet undiscovered particles are expected to exist \cite{Kamionkowski} .

This coincidence is what some refer to as the ``WIMP miracle."  SUSY was proposed in order to solve the hierarchy problem (the large discrepancy between the magnitude of the weak force and the gravitational force) and SUSY could provide elegant means of unifying gravity with the other three fundamental forces \cite{Kamionkowski}. SUSY adds particles to the Standard Model and the lightest of these particles, the neutralino, could be exactly what experimentalists are looking for in the WIMP.  This convergence of SUSY and cosmology is the primary motivation behind the WIMP hypothesis and has launched the dozens of experiments attempting to detect WIMPs \cite{Kamionkowski}.




% EOF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%