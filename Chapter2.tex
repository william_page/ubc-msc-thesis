 % !TEX root =  ./thesis1.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% An example section.
%You should start all sections with \section{SectionName}, which is what will show up
%as the section header, and will be automatically included in the ToC.
 %If you want to reference the section later you should also use
%\label{sec:sectionName}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The WIMP dark matter detection effort is sufficiently large that this thesis will not attempt to review the field of experiments pushing to make the first credible WIMP discovery. Here we quickly mention the leading searches that are constraining dark matter parameters and excluding certain models.

Experimentalists at the LHC attempt to create dark matter through particle collisions, and to observe an absence of energy in their detector as the dark matter particle passes through \cite{LHC}.  Telescopes on earth and aboard satellites seek to observe excess gamma ray signals in nearby dwarf galaxies as a signature of dark matter annihilation in the dense galactic center \cite{LAT}.

Direct detection experiments look for WIMP-nucleon recoils in terrestrial detectors and employ different targets (e.g. liquid argon, liquid xenon, germanium, silicon, calcium tungstate), background rejection techniques, amplifiers, and/or energy thresholds. Direct detection experiments are optimized (or equivalently, limited) to certain WIMP masses and cross sections based on their detector technology. 


\section{Direct Detection}
\label{detwimps}

In order to measure the energy deposited by a WIMP-nucleon collision, detectors must be capable of measuring energies on the order of 1keV.  The (purely classical) expected energy transfer to a target in a WIMP collision is given by:

\begin{equation} \label{collision}
E_{recoil}=(\frac{m_{\chi}m_T}{m_{\chi}+m_{T}})^2\frac{v^2}{m_T}(1-\rm{cos}(\theta_{\chi}))
\end{equation}

\noindent
where $m_\chi$ is the WIMP mass, $m_T$ is the target mass, $v$ is the WIMP velocity, and $\theta_\chi$ is the WIMP scattering angle.  

The WIMP velocity is given by $v$ and deserves brief discussion. In the Standard Halo Model (SHM) the dark matter halo is overall stationary with respect to the galaxy, and therefore the local WIMP velocity is given approximately by the Sun's rotational velocity ($v \approx 220km/s \approx 7 \times10^{-4}\textrm{c}$).  It is standard to model statistical fluctuations in the WIMP velocity, in the galactic rest frame (where the bulk WIMP velocity is zero), by a Maxwellian distribution \cite{lisanti}:
\begin{equation} \label{max}
f(v) =
\begin{cases}
Ae^{(-v^2/v_{0}^2)} & v < v_{esc} \\
0 & v>v_{esc}
\end{cases}
\end{equation}
\noindent where $v_0 = 7 \times10^{-4}\textrm{c}$ and $v_{esc}$ is the local escape velocity ($v_{esc} \approx 4.5\times10^{-3}\textrm{c}$).

One key element of direct detection is made clear from equation \ref{collision}---the WIMP cannot efficiently transfer energy to target components that are much less massive than a nucleon.  Consider the maximum energy transfer of a WIMP-electron collision $(\theta_\chi \rightarrow 180^\circ, \: m_T \approx m_e)$, giving $E_{recoil}\approx 2m_ev^2 =  0.25\textrm{eV}$. Signals of this magnitude will be entirely buried under the noise floor of detectors for even the lowest threshold next generation dark matter experiments (SuperCDMS SNOLAB HV detector thresholds will be on the order of 100eV). 

Instead consider the maximum energy transfer of a WIMP-nucleon collision where the dark matter particle is well matched kinematically to a Ge nucleus target: $(\theta_\chi \rightarrow 180^\circ, \: m_\chi \approx m_T \approx 72 m_p)$.  In this case $E_{recoil} \approx (1/2)m_Tv^2 \approx 16.5\textrm{keV}$, which is certainly an energy capable of detection.

Next, since the local dark matter density is approximately 0.3GeV/cm$^3$ (i.e. many WIMPs streaming through the detectors every day), direct detection experiments hope to measure this steady rate of WIMP events. The expected differential scattering rate, which of course depends on $E_{recoil}$, is given by:
\begin{equation} \label{diff}
\frac{dR}{dE_{recoil}}=\frac{\rho}{m_Tm_{\chi}}\int_{v_{min}}^{\infty}vf(v)\bigg[\frac{d\sigma_{\chi T}}{dE_{recoil}}(v,E_{recoil})\bigg]dv \: \: \: \:\big[\rm{keV \: kg \: day}\big]^{-1}
\end{equation}
\noindent
where $d\sigma_{\chi T}/dE_{recoil}$ is the differential cross section and $v_{min}$ is the minimum WIMP velocity in order to produce recoil energy $E_{recoil}$.\footnote{The rotation of the earth around the sun seasonally adds and subtracts from the WIMP velocity relative to the earth and detecting a seasonal variation in a possible WIMP signal would be another sign that the signal is indeed the dark matter halo.  A different direct detection experiment---the DAMA/LIBRA collaboration---claims that they are seeing this annual modulation in their data \cite{DAMA}.} 

Except for the differential cross section $d\sigma_{\chi T}/dE_{recoil}$ and the WIMP mass, all the parameters of the differential scattering rate are estimated in the SHM.  The differential cross section, while largely unknown, clearly has large implications on the detectability of WIMP particles. The total cross section could be the sum of a spin independent and spin dependent term, although the spin independent term is expected to be amplified. As Witten and Goodman noted in their 1978 paper \cite{witten}, the spin independent term in the cross section scales as the number of nucleons squared, which  greatly increases the likelihood and conceivability of a direct WIMP detection. Even so, lower bounds on the WIMP cross section of most models extend below the coherent neutrino scattering limit shown on figure \ref{limits}. The solar and atmospheric neutrinos to which detectors become sensitive below these WIMP cross sections present a serious obstacle for beyond-next-generation direct detection experiments.

More optimistically, D. Moore computed the integral in equation \ref{diff} for different targets and different cross sections (figure \ref{rate}).  In the (overly optimistic) left plot he used a cross section from disputed WIMP detection claims and in the right plot he used cross sections just below the current published sensitivities. As a rough reference, reading off from figure \ref{rate} (left), a recoil threshold of 6keV gives a rate of $1/10 \: \: [\textrm{events }\textrm{kg}^{-1} \textrm{day}^{-1}]$.  CDMS II had roughly 5kg of detector bulk, translating to a rate of 0.5 events per day.


\begin{figure}
  \begin{center}
    \vspace{-10pt}
    \includegraphics[width=1.\textwidth]{integrated_rate.pdf}
  \end{center}
    \vspace{-12pt}
  \caption{The expected WIMP event rate for the given $m_\chi$ and $\sigma_{SI}=\sigma_{\chi T}$. $\sigma_{SI}$=10$^{-41}$cm$^2$ corresponds to roughly the cross section reported by  DAMA/LIBRA, CRESST, CDMS Si, and CoGent. $\sigma_{SI}$=10$^{-45}$cm$^2$ corresponds to a cross section just below published sensitivities from 2013.  Internal CDMS figure, from \cite{moore}.\label{rate}}
  \vspace{-15pt}
\end{figure}

At these relatively low nuclear-recoil energies and low event-rates, one fundamental challenge to direct detection experiments is background discrimination.  One advantage is that the majority of backgrounds will scatter off elections in the detector bulk. All competitive (with the exception of CDMS HV detectors) direct detection technologies have means to distinguish electron recoils from nuclear recoils and thus reject background events. CDMS's background discrimination technique will be discussed in more detail in chapter 3.   

The above is $in \: principle$ how CDMS and other  direct detection experiments intend to discover WIMPs and measure $\sigma_{\chi T}$ and $m_{\chi}$.  However, ever since 2002 when the first generation of CDMS results were published, no such rate (that has been widely accepted within the community) has been observed.  CDMS has gone through three generations of experiments: CDMS, CDMS II, SuperCDMS Soudan, and is now preparing for SuperCDMS SNOLAB.  Each generation of the experiment has increased the total detector mass and implemented improved detector technology. In two of the iterations the detectors were moved to a cosmogenically cleaner (deeper) and radiogenically cleaner site.  Meanwhile competitor experiments made similar improvements and new detection technologies were developed, all in order to combat the two primary difficulties facing WIMP direct detection: (1) a low rate of WIMP-nucleon collision, and (2) background rejection. 

\begin{figure}
  \begin{center}
    \vspace{-10pt}
    \includegraphics[width=1.\textwidth]{all-mass_projections.png}
  \end{center}
    \vspace{-12pt}
  \caption{Current and projected limits on the WIMP mass-cross section parameter space.  Figure from SuperCDMS collaboration standard public plots \cite{CDMS_plots}.\label{limits}}
  \vspace{-15pt}
\end{figure}

% EOF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%