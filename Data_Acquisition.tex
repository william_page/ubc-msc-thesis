 % !TEX root =  ./thesis1.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% An example section.
%You should start all sections with \section{SectionName}, which is what will show up
%as the section header, and will be automatically included in the ToC.
 %If you want to reference the section later you should also use
%\label{sec:sectionName}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


This chapter rapidly specializes, relative to the discussion of previous chapters, on the topic of Data Acquisition (DAQ) for SuperCDMS SNOLAB. We limit this chapter further by omitting in-depth discussion of the readout electronics and focus on the overall framework and various software elements of the proposed DAQ architecture.

Each iZIP will have a Detector Control and Readout Card (DCRC) connected to the upstream amplifiers and filters of the 12 phonon and 4 charge channels, and it will be continuously digitizing these signals. One substantial upgrade from previous generations of the CDMS electronics is now the existence of deadtime-less triggering and read out. Instead of halting digitization upon read out, the DCRC can simultaneously read out waveforms from triggers and digitize the incoming data stream (further discussion below).

This parallelization of processes is powerful in other ways as well. For example, our DAQ system plans to take advantage of the DCRC's ability to read out very long traces ($\sim $ 50ms) without a livetime penalty. At the SuperCDMS Soudan experiment, excess low frequency noise dominated other sources of TES noise and severely degraded the iZIP energy resolution. As shown in figure \ref{hybrid_cartoon}, the long-trace readout probes low frequencies which will allow us to filter this noise if it exists. Optimally filtering these waveforms is discussed in much further detail in chapter \ref{HOF1}.

 \begin{figure}[h!]
   \centering
   \includegraphics[width=11cm]{hybrid_cartoon.png}
    \caption{Cartoon showing the proposed phonon waveform readout and the ability to monitor low frequency noise. Hybrid waveform readout is discussed in chapter \ref{HOF1}. \label{hybrid_cartoon}}
\end{figure}


The DAQ system also plays an important role in the high statistics calibration runs. These runs, accomplished with different radioactive sources, are critical to understanding the energy signature of background particles and to ensure that a background event will never be mistaken for a WIMP.  We discuss this, and other, fundamental features of the DAQ system below.


\section{The DCRCs}
\label{dcrc}

The DCRC is the fundamental piece of DAQ hardware and these single compact boards have replaced the Front End Boards (FEBs), Readout Trigger Filter (RTF) boards, and bulky digitizers from previous generations of CDMS. The DCRCs continuously digitize the analog signal from the 12 phonon and 4 charge channels at 2.5Mhz (charge) and 625kHz (phonon) and write each channel to a circular memory buffer. This memory buffer is large enough to store several seconds worth of data bins.\footnote{The Rev C version DCRC has a 3.3 second buffer, but the buffer capacity will be larger on later versions.} The digitizer on the DCRC does a 14-bit analog to digital conversion. 


 \begin{figure}[h!]
   \centering
   \includegraphics[width=9cm]{dcrc.png}
    \caption{A Rev C DCRC. The 50-pin connector interfaces with the bias and sensor lines to the detector.  The board receives power over the ethernet and also communicates with the DAQ through the ethernet port.  \label{dcrc}}
\end{figure}

The DCRC is also responsible for determining where triggers occur within the circular buffer and recording the time stamps. Trigger decisions are then made on the data in the circular buffer and triggers are read out before the data are overwritten (discussed further in section \ref{midas}).

A Field Programmable Gate Array (FPGA) on the DCRC allows for a significantly fancier SuperCDMS SNOLAB trigger algorithm than was possible with the previous electronics. The standard trigger algorithm is to look for the rising edge of pulses by setting a threshold and triggering on excursions of the ADC value above the baseline that exceed the threshold.  In experiments seeking to lower thresholds and increase sensitivity to low energy events, there are clear disadvantages to this simple algorithm.  The DAQ system will improve the trigger by integrating into the algorithm information about the noise characteristics and the expected pulse shape.  The computational speed permitted by the FPGA allows for a finite impulse response (FIR) filter to be continuously applied to some finite length of bins in the circular buffer from which a pulse amplitude can be estimated.  The FIR coefficients will be computed based on the noise pulse spectral density (PSD) and pulse shape.

Optimal coefficients for the FIR are given by the optimal filter time domain weights.  While optimal filters are generally thought of in the frequency domain, they can also be computed more efficiently (O$(N)$ vs. O$(N \: \text{log} \: N)$) in the time domain after first inverting a matrix.  As will be discussed in much more depth, equation \ref{eq: pulse_amp} is an example of such a time domain calculation.

\section{MIDAS and the Software Front Ends}
\label{midas}


Immediately downstream of the DCRCs are the software front ends which are written using the MIDAS (Maximum Integrated Data Acquisition System) package.  The front end code is in C++, but the open source MIDAS package comes with a vast array of desirable DAQ features already build in \cite{midas_ref}.

The MIDAS front ends interact with the circular buffer and trigger stamp buffer on the DCRCs and decide which of these data get written to disk.  The cartoon figure \ref{cartoon_frontend} displays this interaction of the front ends with one DCRC, but what it leaves out is that the front ends will be making decisions based on and interacting with data from an entire tower (i.e. 6 DCRCs).  

In the first step, delineated by (1) in the figure, the trigger front end program reads in the time stamps from the trigger buffer.  It decides which triggers to read out based on the time stamps and from which iZIP the trigger occurs.  The most basic of these decisions will reject triggers that occur within some short (and adjustable) time of each other. These piled-up events spoil each other because event energy, position, and timing algorithms in the downstream analysis depend heavily on pulse shape. Figure \ref{pileup} (left) shows two piled-up events.

\begin{figure}[h!]
   \centering
   \includegraphics[width=11cm]{cartoon_frontend.png}
    \caption{A cartoon roughly depicting the communication between the trigger front end, tower front end, the DCRC's trigger buffer, and the DCRC's circular buffer. The hypothetical data stream shows triggers labeled with yellow arrows.  The time stamps of the triggers are continuously recorded in the trigger buffer and periodically read out by the trigger front end. The triggers with yellow arrows too close together would be rejected as piled-up while the other triggers would be read by the tower front end from the circular buffer. \label{cartoon_frontend}}
\end{figure}

The trigger front end programs then transfer the list of time stamps of triggers that have passed the decision algorithms to a second front end, called the tower front end.  In the second step, delineated by (2), the tower front end accesses the DCRC circular buffer and reads out a predetermined (and adjustable) number of bins around all of the triggers.  These waveforms are then written to disk.  

The trigger front end and tower front end communicate with each other in a loop, and communicate with the DCRC when new triggers have been recorded, such that good triggers are never lost. In one iteration of this loop, the process of reading from the circular buffer takes significantly longer than the other steps  combined.  
 
 \section{The DCRC Driver}

The most interesting and powerful aspects of the DCRC and MIDAS relate to detector readout discussed above, but the DCRC and MIDAS are also responsible for detector settings control. The settings fit into 6 general categories: Phonon, Charge, LED, Test Signal, Trigger, and General. Each category holds individual settings; for example, in the Charge category, the user can set the voltage bias to be applied across the detector, and set DC voltage offset of the signal arriving from the charge amplifier. The ``DCRC driver" is the fundamental program that handles the settings control. A section of the DCRC's hard memory is reserved for these settings, which are organized into registers.  Some settings that require a large dynamic range (i.e. many bits) take up multiple registers.  Conversely, because some settings are booleans or a small number of bits, some registers hold many more than 1 setting.  The MIDAS ``Online Database" (ODB) offers a convenient layout of settings for automatic or manual setting adjustment. The ODB is also is the primary means of communication between the user and the DCRC, which is done through the DCRC driver.

The DCRC driver program is written as a MIDAS process.  It should be running whenever the trigger front end and and tower front end (described in section \ref{midas}) are running. The DCRC driver constantly monitors the variables in the 6 categories in the ODB, checking for any change to occur in any of the settings.

The consistent monitoring of variables is done via the MIDAS ``hotlink" functionality, which links any change in a specific ODB variable to a specific function call \cite{midas_ref}. I have written the linked functions, which to first order :

\begin{itemize}
\item{check that the setting has been changed to an allowed valued}
\begin{itemize}
\item{some settings have an allowable range (the detector bias range is -2V$\rightarrow$+2V), while others have discrete allowed values (the driver gain must be 1,2,4, or 8)}
\end{itemize} 
\item{identify and recompute the register corresponding to the setting that has been changed}
\item{through a TCP/IP connection to the DCRC, write the new register value to the DCRC}
\item{if the register is successfully written to, update a special section of ODB (which we label the ``Readback" section), which contains all the current DCRC settings} 
\begin{itemize}
\item{note that this step is necessary because, due to our design choices, the normal section of the ODB in which the user changes settings does not necessarily reflect the current settings on the DCRC}
\item{i.e. if a setting is changed to a disallowed value and therefore rejected or the writing to the DCRC fails, this will not affect the normal section of the ODB}
\end{itemize}
\end{itemize}

Through these steps the DCRC driver successfully propagates any changes in the ODB to the DCRC registers. The DCRC itself then subsequently changes the settings on the detector or to components on the DCRC board.

 
 \section{Detector Testing Tools}
 
The MIDAS ``analyzer" class puts in place a convenient means of real-time viewing of waveforms as MIDAS takes them from the tower front end local memory and writes them to disk. Internal to MIDAS there is a separate separate data buffer, the SYSTEM buffer, through which the writing to hard disk occurs \cite{midas_ref}.  The SYSTEM buffer is stored in Random Access Memory (RAM) and therefore the analyzer class takes advantage of the fast access and reading of the data as it moves through this buffer.


 \begin{figure}[h!]
   \centering
   \includegraphics[width=6.5cm]{pileup1.png}
   \includegraphics[width=5.5cm]{usable_events.png}
    \caption{(Left) Two iZIP phonon pulses that are piled-up. (Right) The rate of usable (non piled-up) events vs. the raw event rate. For this plot a 52ms long trace has been assumed, where an optimal raw rate is $\sim20$Hz giving a $\sim$7Hz usable rate. \label{pileup}}

\end{figure}

GUIs are being written to display the waveforms that are read out, and one of these is shown in figure \ref{pulse_display}. As the GUI displays replace physical oscilloscopes for displaying data, the freedom to cater the GUI design to the needs of test facilities presents significant advantages. This is especially true because SuperCDMS SNOLAB will scale up the number of iZIP/HV detectors substantially (15 to $\sim$50) and the number of phonon channels by an even larger fraction (120 to $\sim$ 600). Each phonon channel, whether for testing or for WIMP search, must be tuned before it becomes operable. Because manual detector tuning is relatively simple but time consuming, the MIDAS GUIs will used to automate this laborious aspect of test facility running.

Currently GUIs are being written to accomplish these automation tasks, namely SQUID tuning, ``IBIS," and ``Complex Impedance" tools. Detailed discussion of these tuning procedures are omitted, however figure \ref{Amy_GUI} shows one of the GUIs already written for user-friendly tuning of the the phonon amplifier components.    

\begin{figure}[h!]
   \centering
   \includegraphics[width=9cm]{Amy_GUI.png}
    \caption{A screen shot of one of the existing detector control GUIs, written primarily by A. Roberts. Internal CDMS figure, used with permission. \label{Amy_GUI}}
\end{figure}

\subsection{The Pulse Display}
\text{I} wrote the Pulse Display GUI (a screen shot of which is shown in figure \ref{pulse_display}), which displays waveforms in the time and frequency domain with oscilloscope-like user control. This is a critical and the fundamental detector testing tool for test facilities.

The GUI is build using ROOT GUI classes. All of the widgets shown in figure \ref{pulse_display} are objects of the numerous ROOT GUI classes (e.g. \sf{TGTextButton} \rm,\sf{TGNumberEntry} \rm, \sf{TGSlider} \rm, \sf{TGTexEntry} \rm, etc.) \cite{root}. The object-oriented C++ framework, off of which ROOT is based, is central to the GUI's functionality. The layout of the GUI is organized in the \sf{GuiMainFrame} \rm class, where the frames, buttons, sliders, check boxes, etc. are given colors, sizes, positions, etc.

An instance of the \sf{GuiMainFrame} \rm class is created in a separate class, \sf{PulseDisplay}\rm, which handles the functionality of the buttons. Similar to the MIDAS ``hotlink" functionality described above, function calls are linked upon the pressing of a button.  A brief description of the buttons is now given, from roughly top to bottom on the GUI.

With the `Detector Selection' button, the user selects which DCRC to see data from. The `Display Fraction' slider selects the fraction of events to display on the screen and the selection is indicated in the number entry to the left of the slider. The necessity of the `Display Fraction' option is discussed below in regard to the throughput capabilities of the GUI.
 
From the `Running Mode' frame, the user has the option to put a restriction on the number of waveforms to be gathered and displayed. In `Free Running' mode (default) there is no restriction but with the `Process $N$ Waveforms' option selected, the number entry will become enabled, allowing the user to choose some number of waveforms to display (default=1000) before stopping the display.

The `Start Run' frame holds some of the most important features, because the main button starts a MIDAS run and starts displaying data. Note that the frontends (triggerfe2, towerfe3) must be running for data to be displayed because the data must be moving through the SYSTEM buffer. Once the button is pressed the text turns to `Stop Run,' and clicking it again stops displaying data and stops the MIDAS run. The `Listen' button starts displaying data by ``listening in" to an already ongoing run. It therefore requires that the frontends are running and that a run has been started. Once the button is pressed the text turns to ``Stop Listen.'' Clicking it again stops displaying data but leaves the run going.

The `Read File Options' frame offers some less-used features.  It initiates a popup window in which the user can select the directory and file name of the MIDAS file to read. The reader expects .mid.gz files (the typical files generated by the MIDAS logger). Clicking the `Read' button starts reading the file. Clicking the right arrow key on the main GUI window iterates waveform-by-waveform through the file.

With the `Channel Selection' frame the user selects the channel to display.  Currently, there�s no way to display charge and phonon channels simultaneously because of the different sample rate of the charge and phonon waveforms.

Pressing the `Capture' button captures the currently displayed trace or PSD and displays it on the screen. This button also enables the two arrows, allowing the user to backtrack over traces PSDs (in case the user didn't press `Capture' sufficiently quickly to see an interesting trace). There is a 10 trace/PSD buffer implemented, and the number entry ``$N$/9'' says where the user is in the buffer. When in `capture mode,' clicking the `Save' button saves the displayed trace to a .root file in the home directory of the Pulse Display.

In the `Display Options' frame, with the `Trace' option selected (default), raw traces are displayed. With the `PSD' option, the power spectral density of the trace is computed by doing a FFT on the trace, and taking the magnitude. Within either of these modes, the `Do Running Average' button can be checked, where a running average of either the traces or the PSDs is computed as the data comes in, and is displayed. The size of the running average can be changed at any time, without having to start from scratch in computing the average.

Clicking `Baseline Subtract' activates a popup window. With both `Subtract Baseline' and `Auto' checked, a running average of the prepulse baseline mean is computed and the entire pulse is subtracted by this amount. The `Auto' check box can be un-checked which stops the computing of the running average, but the pulse is still subtracted by whatever value exists in the running average. This functionality is important in case someone wants to use the DC offset of the ODB and see the change on the Pulse Display. The slider also allows shifting of the trace/PSD.

In the `Y Axis' frame, the different buttons control the ADC Range, which is also controlled by the slider which is parallel to the Y axis. The `Center ADC' displays and controls the ADC value of the center of the Y Axis, which is also controlled by the slider parallel to the Y axis.

Last, in the `Auto Scale' frame, selecting a channel to autoscale and pressing the `AutoScale' button scales the Y-axis to the selected channel.

Behind the scenes of the Pulse Display, it is critical to interface with the MIDAS ``analyzer'' class efficiently in order to for the Pulse Display to operate like an oscilloscope. If significant lag exists between when traces are digitized and when traces are displayed on the screen the user will be looking at old data.  Of course, this lag will typically grow in time, and it is therefore important to identify bottlenecks in the data stream. First, when grabbing traces from the SYSTEM buffer, the code uses the ``GET RECENT'' option so that the most recent data from the buffer is obtained \cite{midas_ref}. Second, even the smallest memory leaks must be eliminated because, with continuous allocation and deallocation of memory for the traces, any memory leak will continue to grow until the machine is ground to a halt.  Third, a bottleneck exists between the the ROOT GUI classes and the display to the monitor. Because display of traces at any rate greater than 60Hz is not generally differentiable from 60Hz to the human eye, the `Display Fraction' option allows the user to adjust the fraction of displayed events, widening the bottleneck, and ensuring that no lag occurs.

\begin{figure}[h!]
   \centering
   \includegraphics[width=12cm]{pulse_display.png}
    \caption{A screen shot of the Pulse Display displaying the PSD of two charge channels in real time.  Behind the scenes, the time domain traces are grabbed from the SYSTEM buffer which are then Fourier transformed, and displayed on the screen. \label{pulse_display}}
\end{figure}

\iffalse
In SQUID tuning, the SQUID bias is adjusted until the SQUID becomes maximally responsive to the current differentials which it is designed to measure. 
``IBIS" and ``Complex Impedance" measurements both serve the same purpose of biasing the TES to an optimal point in its transition. In ``IBIS," the TES bias voltage is adjusted while the current through the TES is measured as in standard operation.  The name ``IBIS" seems to be a small misnomer because, while the read out sensor current is IS, or $I_s$, it is probably more intuitive to think of the TES voltage bias $V_b$ instead of the current through the TES. Nomenclature aside, an IBIS curve is shown in figure \ref{IBIS}, which is truly the I-V curve of a TES. Detailed discussion of the features of the IBIS curve can be found in \cite{pyle}.  The ``Complex Impedance''  an internal  not while the voltage bias (which    . Figure \ref{Amy_GUI}

\begin{figure}[h!]
   \centering
   \includegraphics[width=5cm]{IBIS.png}
      \includegraphics[width=5cm]{dIdV.png}
    \caption{s. \label{IBIS}}
\end{figure}
\fi



\section{Readout Modes}
Calibration runs involve higher event rates than WIMP search runs and therefore we design a DAQ system to handle this throughput. An intelligent DAQ system collects the calibration data quickly, without compromising DM search time. 

In this effort we use different readout methods for the different calibration runs and WIMP search runs. In WIMP search mode (low background), because the event rate is so low, we plan to read out every detector when any trigger is issued from any detector.  In addition, the pileup rejection within the trigger front end will be turned off.

When exposing the detectors to a gamma (Barium) or neutron (Californium) source, the DAQ is put into a ``selective'' readout mode where, for a given trigger, the only detector that is readout is the one that has issued the trigger. Figure \ref{pileup} (right) demonstrates part of the importance of ``selective readout.'' The plotted function is $f(R) = Re^{-R(52\text{ms})}$, which is the rate of non-piled up events in the detector given a raw rate of $R$ events. This derived by noting that the probability that a random event (a poisson process) will occur at least 52ms after a previous event is given by $e^{-R(52\text{ms})}$. Therefore, if we are to calibrate our detectors with an optimal rate, we should create a raw rate of 20Hz, giving a usable rate of 7Hz.  With this relatively high rate (reading out $\sim$1/3 of the total data stream coming from each detector) it is clearly important that ``selective readout'' be implemented, and only the detector issuing the trigger be readout. 
% EOF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
