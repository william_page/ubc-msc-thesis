\contentsline {chapter}{{Abstract}}{ii}{chapter*.1}
\contentsline {chapter}{{Table of Contents}}{iv}{chapter*.3}
\contentsline {chapter}{{List of Tables}}{vi}{chapter*.4}
\contentsline {chapter}{{List of Figures}}{viii}{chapter*.5}
\contentsline {chapter}{{Acknowledgements}}{xi}{chapter*.6}
\contentsline {chapter}{\numberline {1}{Evidence for Dark Matter}}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}{Early Evidence}}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}{Galaxy Clusters}}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}{The Cosmic Microwave Background}}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}{Composition Hypotheses}}{5}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}{The WIMP Hypothesis}}{6}{subsection.1.4.1}
\contentsline {chapter}{\numberline {2}{WIMP Detection}}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}{Direct Detection}}{9}{section.2.1}
\contentsline {chapter}{\numberline {3}{The Cryogenic Dark Matter Search}}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}{Semiconductor Detector Physics}}{13}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}{Electron Recoils}}{14}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}{Nuclear Recoils}}{16}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}{Amplifiers}}{17}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}{Measuring the $e^-/h^+$ Energy}}{17}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}{Measuring the Phonon Energy}}{18}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}{iZIP Interleaved Design}}{18}{section.3.3}
\contentsline {chapter}{\numberline {4}{Data Acquisition}}{21}{chapter.4}
\contentsline {section}{\numberline {4.1}{The DCRCs}}{21}{section.4.1}
\contentsline {section}{\numberline {4.2}{MIDAS and the Software Front Ends}}{23}{section.4.2}
\contentsline {section}{\numberline {4.3}{The DCRC Driver}}{24}{section.4.3}
\contentsline {section}{\numberline {4.4}{Detector Testing Tools}}{26}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}{The Pulse Display}}{27}{subsection.4.4.1}
\contentsline {section}{\numberline {4.5}{Readout Modes}}{30}{section.4.5}
\contentsline {chapter}{\numberline {5}{The Hybrid Optimal Filter}}{32}{chapter.5}
\contentsline {section}{\numberline {5.1}{Fourier Transform of a Hybrid Trace}}{34}{section.5.1}
\contentsline {section}{\numberline {5.2}{Power Aliasing}}{35}{section.5.2}
\contentsline {section}{\numberline {5.3}{Fourier Transform without the FFT}}{36}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}{Fourier Frequencies of a Hybrid Trace}}{38}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}{Time Points of a Hybrid Trace}}{39}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}{The Transformation Matrix: \textbf {A}}}{39}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}{Determining and Inverting the Covariance Matrix}}{40}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}{The Ratio $M/d$}}{42}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}{Robust and Efficient Computation of the Covariance Matrix}}{43}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}{Inversion by Choleski Decomposition}}{45}{subsection.5.4.3}
\contentsline {section}{\numberline {5.5}{Time Domain Covariance Matrix and $\chi ^2$}}{45}{section.5.5}
\contentsline {section}{\numberline {5.6}{Amplitude and Time Offset Estimation Results}}{46}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}{Phonon Pulse Simulation}}{47}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}{Pulse Amplitude Resolution}}{48}{subsection.5.6.2}
\contentsline {section}{\numberline {5.7}{Time Offset Resolution}}{50}{section.5.7}
\contentsline {section}{\numberline {5.8}{Data Volume Consequences}}{50}{section.5.8}
\contentsline {section}{\numberline {5.9}{Conclusion}}{52}{section.5.9}
\contentsline {chapter}{{Bibliography}}{54}{chapter*.7}
\contentsline {part}{{Appendices}}{}{}
\contentsline {chapter}{\numberline {A}{The Imaginary Component of Zero and Nyquist Frequency}}{58}{appendix.A}
\contentsline {chapter}{\numberline {B}{Derivation of Aliasing Frequencies}}{60}{appendix.B}
